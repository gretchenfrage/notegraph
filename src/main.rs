#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use std::{
	env::var,
};
use anyhow::*;
use rocket::Route;
use rocket_contrib::serve::StaticFiles;

#[get("/<name>/<age>")]
fn hello(name: String, age: u8) -> String {
    format!("Hello, {} year old named {}!", age, name)
}

fn static_dir() -> Result<impl Into<Vec<Route>>> {
	var("STATIC_DIR")
		.context("missing var STATIC_DIR")
		.map(StaticFiles::from)
} 

fn main() -> Result<()> {
    let error = rocket::ignite()
    	.mount("/hello", routes![hello])
    	.mount("/public", static_dir()?)
    	.launch();
	Err(error.into())
}